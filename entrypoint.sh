#!/bin/sh

cd /c2metadata/sdtl-reader
dotnet C2Metadata.Web.dll > /tmp/sdtl-reader.log 2>&1  &

cd /usr/local/jetty
java -jar start.jar > /tmp/jetty.log 2>&1 &

cd /usr/src/app
nohup lein run 40000 > /tmp/lein.log 2>&1 &

cd /python-sdtl
python main.py > /tmp/flask.log 2>&1 &

/home/polyglot/entrypoint.sh softwareserver
