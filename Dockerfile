# sdtl-reader
FROM microsoft/aspnetcore-build:2.0.0 AS sdtl-reader

WORKDIR /code

COPY sdtl-reader/src .

RUN dotnet restore C2Metadata.Web
RUN dotnet publish C2Metadata.Web -c Release -r linux-x64 --self-contained true --output /output

# dataset-updater
FROM registry.gitlab.com/c2metadata/dataset-updater-ws:SDTL0.3 as dataset-updater

# codebookutil-web
FROM maven AS codebookutil-web

COPY sdtl-pojo/sdtl-pojo /src/sdtl-pojo
COPY codebookutil-web /src/codebookutil-web

RUN cd /src/sdtl-pojo && mvn install && \
    cd /src/codebookutil-web && mvn install

# stata-sdtl-converter
FROM registry.gitlab.com/c2metadata/stata-sdtl-converter:SDTL0.3 as stata-sdtl-converter

# sas-sdtl
FROM registry.gitlab.com/c2metadata/sas-sdtl:SDTL0.3 as sas-sdtl

# python-to-sdtl
FROM registry.gitlab.com/c2metadata/python-to-sdtl:SDTL0.3 as python-sdtl

# Create softwareserver for polyglot
FROM ncsapolyglot/softwareserver:latest

USER root
RUN apt-get update && apt-get -y install vim libunwind8 python3 python3-pip -o APT::Immediate-Configure=0 && \
        update-alternatives --install /usr/bin/python python /usr/bin/python3 1 && \
        update-alternatives --install /usr/bin/pip pip /usr/bin/pip3 1 && \
        wget http://ftp.br.debian.org/debian/pool/main/o/openssl1.0/libssl1.0.2_1.0.2u-1~deb9u1_amd64.deb && \
        dpkg -i libssl1.0.2_1.0.2u-1~deb9u1_amd64.deb && rm libssl1.0.2_1.0.2u-1~deb9u1_amd64.deb && \
	/bin/sed -i -e 's/^\([^#]*Scripts=\)/#\1/' -e 's/^#\(PythonScripts=\)/\1/' /home/polyglot/polyglot/SoftwareServer.conf && \
        echo "c2metadata" > /home/polyglot/polyglot/scripts/py/.aliases.txt


# Setup sdtl-reader
COPY --from=sdtl-reader /output /c2metadata/sdtl-reader
ENV PATH=${PATH}:/c2metadata/sdtl-reader

RUN curl -s https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor -o /etc/apt/trusted.gpg.d/microsoft.asc.gpg && \
    curl -o /etc/apt/sources.list.d/microsoft-prod.list https://packages.microsoft.com/config/debian/10/prod.list && \
    apt-get update && apt-get install -y aspnetcore-runtime-3.1


# Setup dataset-updater
RUN mkdir -p /usr/local/jetty &&  \
    curl -s https://repo1.maven.org/maven2/org/eclipse/jetty/jetty-distribution/9.4.32.v20200930/jetty-distribution-9.4.32.v20200930.tar.gz | tar xvz -C /usr/local/jetty --strip-components=1

COPY --from=dataset-updater /usr/local/tomcat/webapps/dataset-updater-ws /usr/local/jetty/webapps/dataset-updater-ws
ENV PATH=${PATH}:/usr/local/jetty/bin

# Setup sas-sdtl
COPY --from=sas-sdtl /usr/local/tomcat/webapps/sas-sdtl.war /usr/local/jetty/webapps/sas-sdtl.war

# Setup python-sdtl
COPY --from=python-sdtl /usr/src/app /python-sdtl
RUN cd /python-sdtl && pip install flask waitress

# Setup stata-sdtl-converter
ENV LEIN_VERSION=2.9.3
ENV LEIN_INSTALL=/usr/local/bin/
COPY --from=stata-sdtl-converter /usr/src/app /usr/src/app
RUN cd /tmp && \
    wget -q https://raw.githubusercontent.com/technomancy/leiningen/$LEIN_VERSION/bin/lein-pkg -O $LEIN_INSTALL/lein && \
    chmod 0755 $LEIN_INSTALL/lein && \
    mkdir -p /usr/share/java && \
    wget -q https://github.com/technomancy/leiningen/releases/download/$LEIN_VERSION/leiningen-$LEIN_VERSION-standalone.zip -O /usr/share/java/leiningen-$LEIN_VERSION-standalone.jar && \
    cd /usr/src/app && \
    lein deps

# Setup codebookutil-web
COPY --from=codebookutil-web /src/codebookutil-web/target/codebookutil-web.war /usr/local/jetty/webapps
RUN pip install requests

# Copy converter file to scripts/py folder in container
# this is done to keep cache so you can debug script easily
COPY c2metadata_convert.py /home/polyglot/polyglot/scripts/py/
RUN chown polyglot /home/polyglot/polyglot/scripts/py/c2metadata_convert.py && \
    chmod +x /home/polyglot/polyglot/scripts/py/c2metadata_convert.py

# Custom entrypoint starts services and then polyglot
COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
