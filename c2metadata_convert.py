#!/usr/bin/python3
#C2Metadata (v1.0.0)
#Archive
#zip
#c2metadata.zip

"""
Given an input zipfile containing script and metadata, generate updated metadata and
codebook creating a secondary zipfile.
"""

import logging
import glob
import http.client as http_client
import json
import logging
import os
import requests
import shutil
import subprocess
import sys
import tempfile
import time
from zipfile import ZipFile

logging.basicConfig(level="WARN")

# Various C2Metadata service and parser URLs
# Services are started by entrypoint.sh
UPDATER_SERVICE_URL = "http://localhost:8080/dataset-updater-ws/update/xml"
VARIABLES_SERVICE_URL = (
    "http://localhost:8080/dataset-updater-ws/api/info/variables/names"
)
CODEBOOK_SERVICE_URL = "http://localhost:8080/codebookutil-web"
PARSERS = {
    "python": "http://localhost:8095/api/python-to-sdtl",
    "r": "http://localhost:5000/api/rtosdtl",
    "sas": "http://localhost:8080/sas-sdtl/hello/post",
    "spss": "http://localhost:5000/api/spsstosdtl",
    "stata": "http://localhost:40000/",
}

# Map of script file extensions to languages (lowercase)
ext_lang = {"sps": "spss", "r": "r", "do": "stata", "sas": "sas", "py": "python"}


# Input zip file contains script file and one or more XML files
# Output zip file will contain origina files, updated metadta file
# and codebook.
input_file = sys.argv[1]
logging.debug("Input file: {}".format(input_file))
output_file = sys.argv[2]
logging.debug("Output file: {}".format(output_file))

# Temporary directory where all input/output stored
tmpdir = tempfile.mkdtemp()
logging.debug("Temporary directory: {}".format(tmpdir))

# Read input zipfile and extract to temporary directory
logging.debug("Reading input zip")
with ZipFile(input_file, "r") as zipObj:
    zipObj.extractall(path=tmpdir)

# Read input.json containing input metadata
logging.debug("Reading input.json")
with open(tmpdir + "/input.json") as json_file:
    input_settings = json.load(json_file)

script_file = input_settings["script"]
script_ext = os.path.splitext(script_file)[1][1:]
logging.debug("Script file: {}".format(script_file))
logging.debug("Script extension: {}".format(script_ext))

# Infer language from script extension
language = ext_lang[script_ext.lower()]
logging.debug("Language: {}".format(language))

# Construct dataset descriptions
descriptions = []
logging.debug("Constructing dataset descriptions")
for dataset in input_settings["datasets"]:

    description = {
        "input_file_name": dataset["filename"],
        "DDI_XML_file": dataset["metadata"],
        "file_name_DDI": "",  # TODO: What is it?
        "variables": [],
    }

    if dataset["metadata"]:
        variables = requests.post(
            VARIABLES_SERVICE_URL,
            files={"file": open(tmpdir + "/" + dataset["metadata"], mode="rb")},
        )
        variables.raise_for_status()
        description["variables"] = variables.json()

    descriptions.append(description)

# Read script contents
logging.debug("Reading {} contents".format(script_file))
with open(tmpdir + "/" + script_file, "r") as file:
    code = file.read()

# Construct parser input JSON
parser_input = {"parameters": {"data_file_descriptions": descriptions, language: code}}

# Get SDTL from script
parser = PARSERS[language]
logging.debug("Using parser: {}".format(parser))
logging.debug("Parser input: {}".format(parser_input))

headers = {"Content-type": "application/json"}
sdtl = requests.post(url=parser, data=json.dumps(parser_input), headers=headers)
sdtl.raise_for_status()
sdtl_json = json.loads(sdtl.text)

# Store the SDTL
logging.debug("Writing sdtl.json")
with open(tmpdir + "/sdtl.json", mode="w") as sdtl_file:
    sdtl_file.write(json.dumps(sdtl_json, indent=4))

# Generate inputInfo
input_info = [
    {
        "scriptName": description["input_file_name"],
        "xmlName": description["file_name_DDI"],
        "primary": False,
        "position": None,
    }
    for description in descriptions
]

logging.debug("Writing inputInfo.json")
with open(tmpdir + "/inputInfo.json", "w") as info_file:
    info_file.write(json.dumps(input_info, indent=4))

# Prepare files required by dataset updater
files = [
    ("sdtl", ("sdtl.json", json.dumps(sdtl_json), "application/json")),
    ("inputInfo", ("inputInfo.json", json.dumps(input_info), "application/json")),
]

for dataset in input_settings["datasets"]:
    files.append(
        (
            "datasets",
            (
                dataset["metadata"],
                open(tmpdir + "/" + dataset["metadata"], "rb"),
                "text/xml",
            ),
        )
    )


# Note: In theory the dataset updater service should handle both DDI and EML
# but this is not currently working. Also, it isn't clear whether the
# codebook generator will work with both.

# Get the updated dataset metadata
logging.debug("Getting updated metadata")
resp = requests.post(UPDATER_SERVICE_URL, files=files)
with open(tmpdir + "/updated_metadata.xml", mode="w") as updated_metadata:
    updated_metadata.write(resp.text)

# Get the codebook HTML
logging.debug("Generating codebook")
with open(tmpdir + "/updated_metadata.xml", mode="r") as updated_metadata:
    resp = requests.post(url=CODEBOOK_SERVICE_URL, files={"ddiFile": updated_metadata})
    with open(tmpdir + "/codebook.html", mode="w") as codebook:
        codebook.write(resp.text)

# Create output zipfile
logging.debug("Writing output to {}".format(output_file))
with ZipFile(output_file, "w") as zipObj:
    for folderName, subfolders, filenames in os.walk(tmpdir):
        for filename in filenames:
            filePath = os.path.join(folderName, filename)
            zipObj.write(filePath, os.path.basename(filePath))

# Remove temporary files
shutil.rmtree(tmpdir)
